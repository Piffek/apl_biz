package com.example.api_biz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiBizApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiBizApplication.class, args);
    }
}
